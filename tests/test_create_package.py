import requests

from camtrap_package import package


EXAMPLE_BASE_URL = "https://raw.githubusercontent.com/tdwg/camtrap-dp"
EXAMPLE_VERSION = "1.0-rc.1"

EXAMPLE_DESCRIPTOR_URL = (
    f"{EXAMPLE_BASE_URL}/{EXAMPLE_VERSION}/example/datapackage.json"
)
EXAMPLE_DEPLOYMENTS_URL = (
    f"{EXAMPLE_BASE_URL}/{EXAMPLE_VERSION}/example/deployments.csv"
)
EXAMPLE_MEDIA_URL = f"{EXAMPLE_BASE_URL}/{EXAMPLE_VERSION}/example/media.csv"
EXAMPLE_OBSERVATIONS_URL = (
    f"{EXAMPLE_BASE_URL}/{EXAMPLE_VERSION}/example/observations.csv"
)

# download example metadata and CSV resources
descriptor = requests.get(EXAMPLE_DESCRIPTOR_URL).json()

# drop coverages from descriptor for further tests
cov_spatial = descriptor.pop("spatial")
cov_temporal = descriptor.pop("temporal")
cov_taxonomic = descriptor.pop("taxonomic")

dpack = package.CamTrapPackage(
    metadata=descriptor,
    deployments=EXAMPLE_DEPLOYMENTS_URL,
    media=EXAMPLE_MEDIA_URL,
    observations=EXAMPLE_OBSERVATIONS_URL,
)

valid = dpack.validate_package()

dpack.save()
