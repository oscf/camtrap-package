from dateutil.parser import parse
from camtrap_package.utils import filter_list_of_dicts

### DATAVERSE
### simple camtrap-package -> dataverse mapping


def build_dv_field(ftype, name, value=None, multiple=True):
    if ftype == 1:
        return {
            "typeName": name,
            "multiple": multiple,
            "typeClass": "primitive",
            "value": value,
        }
    if ftype == 2:
        return {
            "typeName": name,
            "multiple": multiple,
            "typeClass": "compound",
            "value": value,
        }
    if ftype == 3:
        return {
            "typeName": name,
            "multiple": multiple,
            "typeClass": "controlledVocabulary",
            "value": value,
        }


def package2dataverse(descriptor, extra_attrs=None, taxonomic_cov=False):
    """
    Maps metadata to the Dataverse format. It does not provide a ready-to-upload
    JSON file but instead it returns all data needed to create `dataset` and
    associated `datafile` objects using Dataverse API.

    Metadata mapping based on:
    http://guides.dataverse.org/en/latest/user/appendix.html#id1

    Args:
    -----
    descriptor (dict):
        Descriptor of already validated camtrap data package.

    Returns:
    --------
    (dict, list)

    Examples:
    ---------
    >>> import json
    >>> descriptor_dict = camtrap_package.package.to_dict()
    >>> dv, files_list = package2dataverse(descriptor_dict)
    >>> with open("dataverse.json", "w") as _file:
    >>>     json.dump(dv, _file, indent=4)
    """

    _const = {
        "subtitle": "Camera Traps Data Package",
        "subject": ["Earth and Environmental Sciences"],
        "kindOfData": ["camera trapping data"],
        "otherReferences": [
            "https://tdwg.github.io/camtrap-dp",
            "https://specs.frictionlessdata.io/data-package",
        ],
    }
    extra_attrs = extra_attrs or _const

    dv_dict = {
        "datasetVersion": {
            "termsOfUse": "",
            "termsOfAccess": "",
            "metadataBlocks": {
                "citation": {"displayName": "Citation Metadata", "fields": []},
                "geospatial": {"displayName": "Geospatial Metadata", "fields": []},
            },
        }
    }
    if taxonomic_cov:
        dv_dict["datasetVersion"]["metadataBlocks"]["taxonomic"] = {
            "displayName": "Taxonomic Metadata",
            "fields": [],
        }

    _d = descriptor
    bdvf = build_dv_field

    date_created = str(parse(_d.get("created")).date())

    ### 1) Dataverse citation
    _dv = dv_dict["datasetVersion"]["metadataBlocks"]["citation"]["fields"]

    platform_url = _d.get("sources", [{}])[0].get("path", "")

    _dv.append(bdvf(1, "title", _d.get("title", ""), False))
    _dv.append(bdvf(1, "subtitle", extra_attrs.get("subtitle", ""), False))
    _dv.append(bdvf(1, "alternativeTitle", _d.get("name", ""), False))
    _dv.append(bdvf(1, "alternativeURL", platform_url, False))
    _dv.append(bdvf(3, "subject", extra_attrs.get("subject", "")))
    _dv.append(
        bdvf(1, "notesText", _d.get("project", {}).get("description", ""), False)
    )
    _dv.append(bdvf(1, "productionDate", date_created, False))
    _dv.append(bdvf(1, "productionPlace", [platform_url], True))
    _dv.append(bdvf(1, "distributionDate", date_created, False))
    _dv.append(bdvf(1, "kindOfData", extra_attrs.get("kindOfData", "")))
    _dv.append(bdvf(1, "otherReferences", extra_attrs.get("otherReferences", "")))
    _dv.append(
        bdvf(1, "dataSources", [k.get("path", "") for k in _d.get("sources", [])])
    )

    # ids
    _dv.append(
        bdvf(
            2,
            "otherId",
            [
                {
                    "otherIdAgency": bdvf(1, "otherIdAgency", platform_url, False),
                    "otherIdValue": bdvf(1, "otherIdValue", _d.get("id", ""), False),
                },
                {
                    "otherIdAgency": bdvf(1, "otherIdAgency", "uuid4", False),
                    "otherIdValue": bdvf(1, "otherIdValue", _d.get("id", ""), False),
                },
            ],
        )
    )

    # software
    _dv.append(
        bdvf(
            2,
            "software",
            [
                {
                    "softwareName": bdvf(1, "softwareName", platform_url, False),
                    "softwareVersion": bdvf(
                        1,
                        "softwareVersion",
                        _d.get("sources", [{}])[0].get("version", ""),
                        False,
                    ),
                }
            ],
        )
    )

    # description
    _dv.append(
        bdvf(
            2,
            "dsDescription",
            [
                {
                    "dsDescriptionValue": bdvf(
                        1, "dsDescriptionValue", _d.get("description", ""), False
                    ),
                    "dsDescriptionDate": bdvf(
                        1, "dsDescriptionDate", date_created, False
                    ),
                }
            ],
        )
    )

    # temporal coverage
    _dv.append(
        bdvf(
            2,
            "timePeriodCovered",
            [
                {
                    "timePeriodCoveredStart": bdvf(
                        1,
                        "timePeriodCoveredStart",
                        _d.get("temporal", {}).get("start", ""),
                        False,
                    ),
                    "timePeriodCoveredEnd": bdvf(
                        1,
                        "timePeriodCoveredEnd",
                        _d.get("temporal", {}).get("end", ""),
                        False,
                    ),
                }
            ],
        )
    )

    # authors
    contributors = _d.get("contributors", [])
    authors = filter_list_of_dicts(
        contributors,
        "role",
        [
            "principalInvestigator",
        ],
    )
    value_author = []
    value_contact = []
    for a in authors:
        value_author.append(
            {
                "authorName": bdvf(1, "authorName", a.get("title", ""), False),
                "authorAffiliation": bdvf(
                    1, "authorAffiliation", a.get("organization", ""), False
                ),
            }
        )
        value_contact.append(
            {
                "datasetContactName": bdvf(
                    1, "datasetContactName", a.get("title", ""), False
                ),
                "datasetContactAffiliation": bdvf(
                    1, "datasetContactAffiliation", a.get("organization", ""), False
                ),
                "datasetContactEmail": bdvf(
                    1, "datasetContactEmail", a.get("email", ""), False
                ),
            }
        )
    _dv.append(bdvf(2, "author", value_author))
    _dv.append(bdvf(2, "datasetContact", value_contact))

    # contributors
    contributors = filter_list_of_dicts(
        contributors, "role", ["contact", "rightsHolder", "publisher", "contributor"]
    )
    value = []
    for c in contributors:
        value.append(
            {"contributorName": bdvf(1, "contributorName", c.get("title", ""), False)}
        )
    _dv.append(bdvf(2, "contributor", value))

    # publications
    publications = _d.get("project", {}).get("references", [])
    value = []
    for p in publications:
        value.append({"publicationURL": bdvf(1, "publicationURL", p, False)})
    _dv.append(bdvf(2, "publication", value))

    # keywords
    keywords = _d.get("keywords", [])
    value = []
    for k in keywords:
        value.append({"keywordValue": bdvf(1, "keywordValue", k, False)})
    _dv.append(bdvf(2, "keyword", value))

    ### 2) Dataverse geospatial
    _dv = dv_dict["datasetVersion"]["metadataBlocks"]["geospatial"]["fields"]
    _dv.append(bdvf(1, "geographicUnit", ["Location (lat/lon coordinates)"]))

    # geographic bbox
    bbox = _d.get("spatial", {}).get("bbox")
    _dv.append(
        bdvf(
            2,
            "geographicBoundingBox",
            [
                {
                    "westLongitude": bdvf(1, "westLongitude", str(bbox[0]), False),
                    "eastLongitude": bdvf(1, "eastLongitude", str(bbox[2]), False),
                    "northLongitude": bdvf(1, "northLongitude", str(bbox[3]), False),
                    "southLongitude": bdvf(1, "southLongitude", str(bbox[1]), False),
                }
            ],
        )
    )

    ### 3) Dataverse taxonomic (custom metadata block)
    if taxonomic_cov:
        _dv = dv_dict["datasetVersion"]["metadataBlocks"]["taxonomic"]["fields"]
        # species
        species = _d.get("taxonomic", [])
        value = []
        for s in species:
            value.append(
                {
                    "scientificName": bdvf(
                        1, "scientificName", s.get("scientificName", ""), False
                    ),
                    "taxonID": bdvf(1, "taxonID", s.get("taxonID", ""), False),
                }
            )
        _dv.append(bdvf(2, "taxonomicCoverage", value))

    ### 4) Datafiles
    datafiles = []
    # 4a) add csv tables
    resources = _d.get("resources")
    for r in resources:
        path = r.get("path")
        if path:
            datafiles.append(
                {
                    "filename": path,
                    "description": r.get("name"),
                    "type": r.get("mediatype"),
                }
            )
    # 4b) add datapackage.json
    datafiles.append(
        {
            "filename": "datapackage.json",
            "description": (
                "Camera trap data package definition file with package-level metadata "
                "and JSON schemas for all csv data tables."
            ),
            "categories": ["Metadata"],
            "type": "application/json",
        }
    )

    return dv_dict, datafiles


def package2zenodo(descriptor):
    """
    Maps package metadata to format necessary to upload it to Zenodo.

    Metadata docs: https://developers.zenodo.org/#representation

    Args:
    -----
    descriptor (dict):
        Descriptor of already validated camtrap data package.

    Returns:
    --------
    (dict, list)

    Examples:
    ---------
    >>> descriptor_dict = dpack.package.to_dict()
    >>> dv, files_list = package2zenodo(descriptor_dict)
    >>> with open("zenodo.json", "w") as _file:
    >>>     json.dump(dv, _file, indent=4)
    """

    # build metadata
    project_metadata = descriptor.get("project", {})
    metadata = {
        "upload_type": "dataset",
        "title": project_metadata.get("title"),
        "description": project_metadata.get("description")
        or "Camera Traps Data Package",
        "access_right": "closed",
        "keywords": descriptor.get("keywords", []),
        "notes": "https://tdwg.github.io/camtrap-dp",
        "version": descriptor.get("version", "1.0"),
    }

    contributors = descriptor.get("contributors", [])
    authors = filter_list_of_dicts(
        contributors,
        "role",
        [
            "principalInvestigator",
        ],
    )
    creators_list = []
    for c in authors:
        author_dict = {"name": c["title"]}
        organization = c.get("organization")
        if organization:
            author_dict["affiliation"] = organization
        creators_list.append(author_dict)
    metadata["creators"] = creators_list

    dates_dict = descriptor.get("temporal")
    if dates_dict:
        dates_dict["type"] = "Collected"
        metadata["dates"] = [dates_dict]

    other_contributors = filter_list_of_dicts(
        contributors,
        "role",
        ["contact", "rightsHolder", "publisher", "contributor"],
    )
    contributor_type_mapper = {
        "contact": "ContactPerson",
        "rightsHolder": "RightsHolder",
        "publisher": "Other",
        "contributor": "Other",
    }
    other_contributors_list = []
    for c in other_contributors:
        con_dict = {"name": c["title"], "type": contributor_type_mapper[c["role"]]}
        organization = c.get("organization")
        if organization:
            con_dict["affiliation"] = organization
        other_contributors_list.append(con_dict)
    if other_contributors_list:
        metadata["contributors"] = other_contributors_list

    metadata_dict = {"metadata": metadata}

    # build files list
    datafiles = []
    resources = descriptor.get("resources")
    for r in resources:
        path = r.get("path")
        if path:
            datafiles.append(path)
    datafiles.append("datapackage.json")

    return metadata_dict, datafiles
