# -*- coding: utf-8 -*-
"""
"""
import json
import os
import tempfile
import uuid
import webbrowser
import zipfile
from string import Template

import requests
import yaml
from frictionless import Package, Schema
from pandas import DataFrame, read_csv, to_datetime

from camtrap_package import templates
from camtrap_package.utils import deep_merge, get_timestamp, generate_profile_template


class CamTrapPackage:
    """
    Base class for the standardized camera trap data package model. See:
    https://tdwg.github.io/camtrap-dp
    https://specs.frictionlessdata.io/data-package
    """

    # Private attributes
    __schema_url = "https://raw.githubusercontent.com/tdwg/camtrap-dp/{version}"
    __profile = "camtrap-dp-profile.json"
    __schema_dep = "deployments-table-schema.json"
    __schema_med = "media-table-schema.json"
    __schema_obs = "observations-table-schema.json"
    __schemas = {
        "deployments": __schema_dep,
        "media": __schema_med,
        "observations": __schema_obs,
    }

    def __init__(
        self,
        metadata,
        deployments="deployments.csv",
        media="media.csv",
        observations="observations.csv",
        schema_version="1.0-rc.1",
        base_path=None,
        schema_url=None,
        sync_schemas=False,
        taxon_ids=None,
    ):
        """
        Init a CamTrapPackage() class.

        Args:
        -----
        metadata (str, dict):
            path to yaml file or dict containing package metadata
        deployments (str, DataFrame):
            Url or path to CSV file or pandas DataFrame with deployments table.
        media (str, DataFrame):
            Url or path to CSV file or pandas DataFrame with multimedia table.
        observations (str, DataFrame):
            Url or path to CSV file or pandas DataFrame with observations table.
        base_path (str):
            Base path to csv tables.
        schema_url (str):
            Url to a repository containing data package profile and JSON schemas
            for all tables.
        schema_version (str):
            JSON schema version.
        sync_schemas (bool):
            Synchronize resource schemas with input tables (it takes care about
            required attributes).
        taxon_ids (dict or None):
            Dictionary where keys are latin taxon names and values are full reference urls containing taxon id.

        Returns:
        --------
        None
        """
        self.schema_url = schema_url or self.__schema_url
        self.schema_version = schema_version
        self.schema_profile_url = os.path.join(
            self.schema_url.format(version=self.schema_version), self.__profile
        )
        # get absolute path to base_path
        if base_path:
            base_path = os.path.abspath(base_path)
        self.base_path = base_path
        self.valid_package = False
        self.tables = None
        self.package = None
        self.report = None

        # generate package profile template
        self.template = generate_profile_template(
            self.schema_profile_url, self.schema_version
        )

        # set profile schema
        self.template["profile"] = self.schema_profile_url
        self.template["created"] = get_timestamp()

        # set tables schemas
        self.tables_schemas = {}
        for i, resource in enumerate(self.template["resources"]):
            resource_schema = os.path.join(
                self.schema_url.format(version=self.schema_version),
                self.__schemas[resource["name"]],
            )
            resource["schema"] = resource_schema
            self.tables_schemas[resource["name"]] = resource_schema

        # load resources to pandas dataframes
        self.tables = {
            "deployments": self.load_resource(deployments, base_path),
            "media": self.load_resource(media, base_path),
            "observations": self.load_resource(observations, base_path),
        }

        # NOTE: in frictionless 5 Package cannot be created from invalid metadata
        self.load_metadata(metadata)
        self.add_coverages(taxon_ids=taxon_ids)
        self.package = Package(self.template)

        # switch to a working directory
        if not self.base_path:
            self.base_path = tempfile.mkdtemp()
            # copy tables to a working space
            for name, dataframe in self.tables.items():
                dataframe.to_csv(
                    os.path.join(self.base_path, name + ".csv"), index=False
                )
        os.chdir(self.base_path)

        if sync_schemas:
            self.sync_resource_schemas()

    def sync_resource_schemas(self):
        for i, resource in enumerate(self.package["resources"]):
            resource_name = resource["name"]
            schema_url = resource["schema"]
            schema_content = json.loads(requests.get(schema_url).content)
            schema = Schema.from_descriptor(schema_content)
            new_schema = Schema()
            data_fields = self.tables[resource_name].columns
            # iterate over schema's fields and check if all required
            # fields are in a table
            for field in schema.fields:
                required = field.get("constraints", {}).get("required", False)
                if required and field["name"] not in data_fields:
                    raise Exception(
                        f"A required field {field} not present in "
                        f"{resource_name} table."
                    )
                if field["name"] in data_fields:
                    new_schema.add_field(field)
            self.package["resources"][i]["schema"] = new_schema

    @staticmethod
    def load_resource(resource, base_path=None):
        """
        Loads a resource from url or given path to a pandas dataframe.
        """
        if isinstance(resource, DataFrame):
            return resource
        if base_path:
            resource = os.path.join(base_path, resource)
        # preserve and not interpret dtype; this will be validated later
        return read_csv(resource, dtype=object)

    def load_metadata(self, metadata):
        """
        Loads provided metadata. There is no need for validation here as
        this will be done later (both package-level metadata and each
        data table will be validated).

        Args:
        -----
        metadata (str, dict):
            Dictionary or path to a YAML file with a package-level metadata.

        """
        # load user-defined package-level metadata
        if isinstance(metadata, str):
            metadata = yaml.load(open(metadata, "rb"), Loader=yaml.Loader)

        # update package with provided metadata
        deep_merge(self.template, metadata or {})

        return

    def print_errors(self):
        if self.valid_package:
            return
        if self.report.errors:
            return self.report.errors
        elif self.report.tasks:
            return self.report.tasks

    def validate_package(self):
        self.report = self.package.validate()
        self.valid_package = self.report.valid
        if self.valid_package:
            for resource_name in self.tables.keys():
                resource = self.package.get_resource(resource_name)
                # include hash
                resource.hash = resource.stats.md5
        return self.valid_package

    def get_descriptor(self):
        # replace full schemas with schema urls
        descriptor = self.package.to_descriptor()
        for resource in descriptor["resources"]:
            try:
                resource["schema"] = self.tables_schemas[resource["name"]]
            except KeyError:
                # if resource is not in self.tables_schemas it is a custom table
                continue
        return descriptor

    def show_report(self, supress_warnings=True):
        """
        Simple method to visualize frictionless report in a webbrowser. Based on:
        https://components.frictionlessdata.io/?path=/story/frictionless-components--page
        """
        if self.report:
            report = self.report
            if supress_warnings:
                report.warnings = []
            html = templates.HTML_REPORT
            report_str = json.dumps(report.to_dict())
            html = Template(html).safe_substitute(report=report_str)
            with open("report.html", "w") as _file:
                _file.write(html)
            webbrowser.open("file://" + os.path.realpath("report.html"))

    def add_coverages(self, taxon_ids=None):
        """
        Update package-level metadata with a spatial, temporal and taxonomic
        coverage.
        """
        self.add_spatial_coverage()
        self.add_temporal_coverage()
        self.add_taxonomic_coverage(taxon_ids=taxon_ids)

    def add_spatial_coverage(self):
        """
        https://tools.ietf.org/html/rfc7946
        """
        deployments = self.tables["deployments"]
        xmin = float(deployments.longitude.min())
        xmax = float(deployments.longitude.max())
        ymin = float(deployments.latitude.min())
        ymax = float(deployments.latitude.max())
        geojson = {
            "type": "Polygon",
            "bbox": [xmin, ymin, xmax, ymax],
            "coordinates": [
                [
                    [xmin, ymin],
                    [xmax, ymin],
                    [xmax, ymax],
                    [xmin, ymax],
                    [xmin, ymin],
                ]
            ],
        }
        self.template["spatial"] = geojson

    def add_temporal_coverage(self):
        """ """
        deployments = self.tables["deployments"]
        from_date_df = to_datetime(deployments["deploymentStart"])
        to_date_df = to_datetime(deployments["deploymentEnd"])

        from_date = from_date_df.min().strftime("%Y-%m-%d")
        to_date = to_date_df.max().strftime("%Y-%m-%d")
        temporal = {"start": from_date, "end": to_date}
        self.template["temporal"] = temporal

    def add_taxonomic_coverage(self, taxon_ids=None):
        """ """
        observations = self.tables["observations"]
        species = (
            observations.loc[
                observations["scientificName"].notnull(),
                ["scientificName"],
            ]
            .drop_duplicates()
            .sort_values(by="scientificName")
        )
        if taxon_ids:
            species["taxonID"] = species["scientificName"].apply(
                lambda x: taxon_ids.get(x, "")
            )
        species = species.to_dict("records")
        self.template["taxonomic"] = species

    def save(
        self,
        output_path=None,
        sort_keys=False,
        make_archive=True,
        generate_uuid4=True,
    ):
        """ """
        if not self.valid_package:
            return False

        # mkdir if output_path does not exist
        if output_path:
            os.makedirs(output_path, exist_ok=True)
            os.chdir(output_path)
        else:
            output_path = self.base_path

        descriptor = self.get_descriptor()

        # generate uuid4 (if requested)
        if generate_uuid4:
            descriptor["id"] = str(uuid.uuid4())

        # dump descriptor
        with open("datapackage.json", "w") as _file:
            json.dump(descriptor, _file, indent=4, sort_keys=sort_keys)

        # create zipfile (if requested)
        zip_name = "{name}.zip".format(name=self.package.name)
        if make_archive:
            with zipfile.ZipFile(zip_name, "w") as zipf:
                zipf.write(
                    os.path.join(self.base_path, "deployments.csv"),
                    arcname="deployments.csv",
                )
                zipf.write(
                    os.path.join(self.base_path, "media.csv"), arcname="media.csv"
                )
                zipf.write(
                    os.path.join(self.base_path, "observations.csv"),
                    arcname="observations.csv",
                )
                zipf.write("datapackage.json")
        return True
