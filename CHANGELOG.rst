=========
Changelog
=========

Version 0.1.5
=============

- Upgrade to Camtrap DP 1.0-rc.1
- Upgrade to frictionless-py 5.14.5

Version 0.1.4
=============

- Upgrade to Camtrap DP 0.1.6
- Upgrade to frictionless-py 4.18.0

Version 0.1.3
=============

Version 0.1.2
=============
  
Version 0.1.1
=============
  
Version 0.1.0
=============

- Initial commit
